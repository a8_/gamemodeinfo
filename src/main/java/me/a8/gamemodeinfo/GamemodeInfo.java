package me.a8.gamemodeinfo;
import me.a8.gamemodeinfo.commands.gi;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public final class GamemodeInfo extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {

        //TODO: make startup message colored
        int pluginId = 6474;
        Metrics metrics = new Metrics(this, pluginId);

        System.out.println("Gamemode-Info has now started up");
        System.out.println("version 1");

        getServer().getPluginManager().registerEvents( this, this);
        getCommand("gi").setExecutor(new gi());

    }




    @Override
    public void onDisable() {
        //TODO: make shutdown message colored
        System.out.println("Gamemode-info has now shut down");
    }
}
